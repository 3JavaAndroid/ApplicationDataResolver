package sda3.amen.com.moviescontentresolver;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddActivity extends AppCompatActivity {

    private static final String uriString =
            "content://sda3.amen.com.moviesdatabase.providers.MoviesContentProvider/movies";
    private static final Uri uri = Uri.parse(uriString);

    @BindView(R.id.editTitle)
    protected EditText editText;

    @BindView(R.id.editDuration)
    protected EditText editDuration;

    private String id;

    @OnClick(R.id.buttonSave)
    public void onSave() {
        String title = editText.getText().toString();
        int duration = Integer.parseInt(editDuration.getText().toString());

        ContentValues values = new ContentValues();
        values.put("name", title);
        values.put("duration", duration);

        ContentResolver resolver = getContentResolver();

        if (!getIntent().hasExtra("item")) {
            String resultUrl = resolver.insert(uri, values).toString();

            Toast.makeText(this, resultUrl, Toast.LENGTH_SHORT).show();
        } else {
            int update = resolver.update(uri, values, "id=?", new String[]{
                    getIntent().getStringExtra("item").split(";")[0]});

            Toast.makeText(this, "Updated: " + update, Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("item")) {
            editText.setText(getIntent().getStringExtra("item").split(";")[1]);
            editDuration.setText(getIntent().getStringExtra("item").split(";")[2]);
        }
    }
}
