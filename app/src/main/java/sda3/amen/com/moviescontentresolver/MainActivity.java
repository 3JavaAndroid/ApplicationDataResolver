package sda3.amen.com.moviescontentresolver;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends AppCompatActivity {
    private static final int INDEX_ID = 0;
    private static final int INDEX_TITLE = 1;
    private static final int INDEX_DURATION = 2;

    private static final String uriString =
            "content://sda3.amen.com.moviesdatabase.providers.MoviesContentProvider/movies";
    private static final Uri uri = Uri.parse(uriString);

    @BindView(R.id.listView)
    protected ListView listView;

    private ArrayAdapter<String> listAdapter;

    @OnItemLongClick(R.id.listView)
    protected boolean removeMovie(int position) {
        String id = listAdapter.getItem(position).split(";")[0];
        ContentResolver resolver = getContentResolver();

        int i = resolver.delete(uri,
                "id=?",
                new String[]{id});
        Toast.makeText(this, "Removed " + i + " rows", Toast.LENGTH_SHORT).show();
        refreshMovies();
        return true;
    }

    @OnItemClick(R.id.listView)
    protected void editItem(int position) {
        Intent i = new Intent(this, AddActivity.class);
        i.putExtra("item", listAdapter.getItem(position));
        startActivity(i);
    }

    @OnClick(R.id.button)
    public void newMovie() {
        Intent i = new Intent(this, AddActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                new ArrayList<String>());

        listView.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshMovies();
    }

    private void refreshMovies() {
        listAdapter.clear();
        // getting uri from uri string
        ContentResolver resolver = getContentResolver();

        // creating cursor
        Cursor cursor = resolver.query(uri, null, null, null, null);
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();

            int id = cursor.getInt(INDEX_ID);
            String title = cursor.getString(INDEX_TITLE);
            int duration = cursor.getInt(INDEX_DURATION);

            listAdapter.add(id + ";" + title + ";" + duration);
        }

    }
}